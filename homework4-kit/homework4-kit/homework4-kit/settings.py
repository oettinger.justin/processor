# ======= GENERAL SETTINGS: Adapt these per assignment =============
ASSIGNMENT = "Duke ECE/CS 250 Homework 4"
SEMESTER = "Fall 2018"
TEST_DIR = 'tests' # where test files are located
IS_GRADER = False # if true, we'll expect to see a 'points' parameter in test setup

SPIM_BINARY = 'spim' # location of spim executable (or just 'spim' if in path); only needed if mode is SPIM
LOGISIM_JAR = 'logisim_cli.jar' # location of logisim_cli.jar; only needed if mode is LOGISIM

# modes: 
#   EXECUTABLE - for C programs, etc.
#   SPIM       - run with command line spim
#   LOGISIM    - run with logisim command-line front-end
MODE = 'LOGISIM'

NON_ZERO_EXIT_STATUS_PENALTY = 0.75 # multiply score by this if exit status is non-zero
VALGRIND_PENALTY = 0.5 # multiply score by this if a valgrind test showed a leak

# ============= TEST SETUP ========================
force_suite_filename = 'hw4.circ'
suite_names = ['simple', 'boolean', 'arithmetic', 'shift', 'memory', 'control', 'io' ]
suites = {
    "simple": [
        { "desc": "addi basics",                       "args": ['-c 10 -ic 1,reset=1:2,reset=0 -lo tests/simple.imem.lgsim -la tests/simple.dmem.lgsim'], 'command_append': "| grep -E -v '(probe         |/ )'" },
    ],
    "boolean": [
        { "desc": "not, xor instrctions",              "args": ['-c 10 -ic 1,reset=1:2,reset=0 -lo tests/boolean.imem.lgsim -la tests/boolean.dmem.lgsim'], 'command_append': "| grep -E -v '(probe         |/ )'" },
    ],
    "arithmetic": [
        { "desc": "addi, add, sub instructions",       "args": ['-c 10 -ic 1,reset=1:2,reset=0 -lo tests/arithmetic.imem.lgsim -la tests/arithmetic.dmem.lgsim'], 'command_append': "| grep -E -v '(probe         |/ )'" },
    ],
    "shift": [
        { "desc": "sll, srl instructions",             "args": ['-c 10 -ic 1,reset=1:2,reset=0 -lo tests/shift.imem.lgsim -la tests/shift.dmem.lgsim'], 'command_append': "| grep -E -v '(probe         |/ )'" },
    ],
    "memory": [
        { "desc": "lw, sw instructions",               "args": ['-c 20 -ic 1,reset=1:2,reset=0 -lo tests/memory.imem.lgsim -la tests/memory.dmem.lgsim'], 'command_append': "| grep -E -v '(probe         |/ )'" },
    ],
    "control": [
        { "desc": "blt, bne, jal, j, jr instructions", "args": ['-c 12 -ic 1,reset=1:2,reset=0 -lo tests/control.imem.lgsim -la tests/control.dmem.lgsim'], 'command_append': "| grep -E -v '(probe         |/ )'" },
    ],
    "io": [
        { "desc": "input, output instructions",        "args": ['-lo tests/io.imem.lgsim -la tests/io.dmem.lgsim -lk tests/io.buffer -c 20 -tty full'], 'command_append': "| grep -E -v '(probe         |/ )'" },
    ],
}
