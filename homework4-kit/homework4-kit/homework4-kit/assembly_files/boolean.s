.text
# test not, xor instrctions

#nand
addi $r1, $r0, 15
addi $r2, $r0, 12
not $r3, $r2             # ~0x000c == 0xfff3
 
#xor
xor $r4, $r2, $r1        # 0xf ^ 0xc == 0x3

halt

.data
